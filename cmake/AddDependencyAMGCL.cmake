#------------- Add AMGLC Dependency -------------#
#-this download AMGLC 0.6.4 and patches it for use ----#

INCLUDE(ExternalProject)
ExternalProject_Add(amgcl
  URL             https://github.com/ddemidov/amgcl/archive/0.6.4.tar.gz
  PREFIX          ${CMAKE_BINARY_DIR}/amgcl
  CMAKE_ARGS      -DCMAKE_INSTALL_PREFIX=${CMAKE_BINARY_DIR}/amgcl/install
  PATCH_COMMAND   patch CMakeLists.txt < ${PROJECT_CMAKE_DIR}/amgcl_patch.diff
  CONFIGURE_COMMAND ""
  BUILD_COMMAND     ""
  INSTALL_COMMAND   ""
)
ExternalProject_Get_Property(amgcl source_dir)
SET(AMGCL_INCLUDE_DIRS ${source_dir})
ADD_LIBRARY(AMGCL INTERFACE)
TARGET_INCLUDE_DIRECTORIES(AMGCL
  INTERFACE
    $<BUILD_INTERFACE:${AMGCL_INCLUDE_DIRS}>
    $<INSTALL_INTERFACE:${INSTALL_INCLUDE_DIR}/3rdParty/AMGCL>
)

INSTALL( DIRECTORY ${AMGCL_INCLUDE_DIRS}/amgcl
  DESTINATION "${INSTALL_INCLUDE_DIR}/3rdParty/AMGCL" )

INSTALL(TARGETS AMGCL
  EXPORT ${PROJECT_NAME}Targets
  COMPONENT Devel
)